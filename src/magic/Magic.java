package magic;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tetem
 */
public class Magic {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int opcion = 3;
        while (opcion != 0) {
            System.out.println("Menú");
            System.out.println("1. Leer fichero Json");
            System.out.println("2. Añadir nueva carta");
            System.out.println("3. Salir");
            opcion = teclado.nextInt();
            switch (opcion) {
                case 1: {
                    readJson();
                    break;
                }
                case 2: {
                    addCard(teclado);
                    break;
                }
                case 3: {
                    System.exit(0);
                }
            }
        }
    }

    public static void readJson() {
        Gson gson;
        File f = null;
        FileReader reader = null;
        Carta[] cartas;

        try {
            f = new File("C:/Users/tetem/Desktop/cartas.json");
            reader = new FileReader(f);
            gson = new Gson();
            JsonReader jsonReader = new JsonReader(reader);
            cartas = gson.fromJson(jsonReader, Carta[].class);
            for (int i = 0; i < cartas.length; ++i) {
                System.out.println(cartas[i].toString());
            }
        } catch (FileNotFoundException ex) {
            System.err.println("Error al acceder al fichero de configuracion: " + ex.toString());
        } finally {
            try {
                reader.close();
            } catch (IOException ex) {
                System.err.println("Error al cerrar el fichero de configuracion: " + ex.toString());
            } catch (NullPointerException ex) {
                System.err.println("El reader nunca fue creado: " + ex.toString());

            }
        }
    }

    public static void addCard(Scanner teclado) {
        String nombre;
        String tipo;
        String[] subtipo;
        String reglas;
        String historia;
        int ataque;
        int defensa;
        FileWriter writer = null;
        FileReader reader=null;
        Carta[] cartas;
        teclado.nextLine();
        System.out.println("Introduce el nombre de la carta");
        nombre = teclado.nextLine();
        System.out.println("Introduce el tipo de carta");
        tipo = teclado.nextLine();
        System.out.println("¿Cuántos subtipos tiene?");
        int cantidad = teclado.nextInt();
        subtipo = new String[cantidad];
        for (int i = 0; i < cantidad; ++i) {
            System.out.println("introduce el subtipo nº " + (i + 1));
            subtipo[i] = teclado.next();
        }
        teclado.nextLine();
        System.out.println("Introduce las reglas de la carta");
        reglas = teclado.nextLine();
        System.out.println("Introduce la historia de la carta");
        historia = teclado.nextLine();
        System.out.println("Introduce el ataque");
        ataque = teclado.nextInt();
        System.out.println("Introduce la defensa");
        defensa = teclado.nextInt();
        //Añadimos la carta al archivo
        try {
            File f = new File("C:/Users/tetem/Desktop/cartas.json");      
            Gson gson = new Gson();
            reader = new FileReader(f);
            JsonReader jsonReader = new JsonReader(reader);
            cartas = gson.fromJson(jsonReader, Carta[].class);
            Carta[] aux=new Carta[cartas.length+1];
            Carta cartita = new Carta(nombre, tipo, subtipo, reglas, historia, ataque, defensa);
            for (int i=0;i<cartas.length;++i){
                aux[i]=cartas[i];
            }
            aux[cartas.length]=cartita;
            writer = new FileWriter(f);
            gson.toJson(aux, writer);
        } catch (FileNotFoundException ex) {
            System.err.println("Error al crear el fichero: " + ex.toString());
        } catch (IOException ex) {
            System.err.println("Error de escritura: " + ex.toString());
        } finally {
            try {
                writer.close();
            } catch (IOException ex) {
                System.err.println("Error al cerrar el fichero de configuracion: " + ex.toString());
            } catch (NullPointerException ex) {
                System.err.println("El reader nunca fue creado: " + ex.toString());
            }
        }
    }
}
