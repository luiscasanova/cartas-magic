package magic;

/**
 *
 * @author tetem
 */
public class Carta {
    private String nombre;
    private String tipo;
    private String[] subtipo;
    private String reglas;
    private String historia;
    private int ataque;
    private int defensa;


public Carta(String nombre, String tipo,String[] subtipo, String reglas,String
            historia, int ataque, int defensa){
    this.nombre=nombre;
    this.tipo=tipo;
    this.subtipo=subtipo;
    this.reglas=reglas;
    this.historia=historia;
    this.ataque=ataque;
    this.defensa=defensa;
}

@Override
public String toString(){
    String subs="";
    for (int i=0;i<this.subtipo.length;++i){
        subs=subs+subtipo[i]+" ";
    }
   return ("Nombre: "+nombre+", tipo de carta: "+tipo+ ", subtipos: "+subs+"\nreglas: "+reglas); 
}
}